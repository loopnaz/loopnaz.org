/**
 * @file Shortcode for navigation links
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @see {@link https://www.11ty.dev/docs/shortcodes Shortcodes in Eleventy}
 */

// Import local filters
var fileToString = require('../filters/file-to-string')

/**
 * Define markup for a navigation item
 * @module _includes/shortcodes/nav-item
 * @since 2.0.0
 * @param {Object} item Subset of Eleventy `data` in the `nav` collection
 * @return {string} HTML
 * @example
 * // In an Eleventy template
 * `${this.navItem(item)}`
 */
module.exports = ({data: {icon, navTitle, url}, ...rest}) => `<!--_includes/shortcodes/nav-item.js-->
<li>
  <a href="${url ? url : rest.url}">
    ${icon
    ? `<span>${fileToString(icon)}</span>
<span>${navTitle}</span>` 
    : navTitle}
  </a>
</li>`
