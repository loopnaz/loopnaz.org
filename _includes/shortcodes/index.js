/**
 * @file Loader file for custom shortcodes
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Configure imported shortcodes with Eleventy
 * @module _includes/shortcodes
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/config/ Configuring Eleventy}
 * @see {@link https://www.11ty.dev/docs/shortcodes Shortcodes in Eleventy}
 */
module.exports = {
  navItem: require('./nav-item')
}
