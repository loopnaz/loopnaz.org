/**
 * @file Transform to minify HTML template files
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @see {@link https://www.11ty.dev/docs/config/#transforms Transforms in Eleventy}
 */

/*
 * Import Juriy Zaytsev’s HTMLMinifier module
 * @see {@link https://github.com/kangax/html-minifier HTMLMinifier on GitHub}
 */
var {minify} = require('html-minifier-terser')

/**
 * Eleventy minifies HTML automatically in production
 * @module _includes/transforms/minify-html
 * @since 2.0.0
 * @param {string} content An HTML document
 * @param {string} outputPath Where Eleventy should output the content
 * @return {string} The minified content
 * @see {@link https://www.11ty.dev/docs/data-js/#example-exposing-environment-variables Environment variables in Eleventy}
 */
module.exports = async (content, outputPath) => {
  // Only minify HTML for production
  if(process.env.ELEVENTY_ENV === 'production' &&
    (outputPath !== false && outputPath.endsWith('.html'))) {
    var result = await minify(content, {
      useShortDoctype: true,
      removeComments: true,
      collapseWhitespace: true
    })

    return result
  }

  return content
}

