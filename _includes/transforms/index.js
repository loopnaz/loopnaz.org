/**
 * @file Loader file for custom transforms
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 */

/**
 * Configure imported transforms with Eleventy
 * @module _includes/transforms
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/config/ Configuring Eleventy}
 * @see {@link https://www.11ty.dev/docs/config/#transforms Transforms in Eleventy}
 */
module.exports = {
  minifyHTML: require('./minify-html')
}
