/**
 * @file Filter to minify JavaScript inline
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @see {@link https://www.11ty.dev/docs/filters Filters in Eleventy}
 */

/*
 * Import Terser module
 * @see {@link https://github.com/terser-js/terser GitHub}
 */
var {minify} = require('terser')

/**
 * Minify JavaScript
 * @module _includes/filters/minify-js
 * @since 2.0.0
 * @param {string} script A JavaScript file’s contents
 * @return {string} The minified script
 * See {@link https://www.11ty.dev/docs/quicktips/inline-js/ Eleventy Quicktip}
 * @see {@link https://www.11ty.dev/docs/data-js/#example-exposing-environment-variables Environment variables in Eleventy}
 * @example
 * // In an Eleventy template
 * `${await this.minifyJS($this.fileToString('js/file.js'))}`
 */
module.exports = async script => {
  // Only minify scripts for production
  if(process.env.ELEVENTY_ENV === 'production') {
    try {
      var minified = await minify(script)

      return minified.code
    }

    catch (err) {
      console.error('Terser error: ', err)

      return script
    }
  }

  return script
}
