/**
 * @file Loader file for custom filters
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @see {@link https://www.11ty.dev/docs/filters Filters in Eleventy}
 */

/**
 * Configure imported filters with Eleventy
 * @module _includes/filters
 * @since 2.0.0
 */
module.exports = {
  fileToString: require('./file-to-string'),
  minifyCSS: require('./minify-css'),
  minifyJS: require('./minify-js'),
}
