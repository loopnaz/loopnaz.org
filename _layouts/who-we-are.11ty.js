/**
 * @file “Who We Are” page layout
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

// Import packages
var EleventyFetch = require('@11ty/eleventy-fetch');
var {stripHtml} = require('string-strip-html')

/**
 * Front matter data
 */
exports.data = {
  layout: 'page',
  tags: 'discover',
  templateEngineOverride: '11ty.js,md',
  weight: 1,
  eleventyComputed: {
    json: async data => {
      try {
        var res = await EleventyFetch(data.beliefsUrl, {
          duration: '30d', // Save for 30 days
          type: 'json'
        })
        return res
      } catch(error) {
        return data[data?.page.lang]?.beliefs
      }
    }
  }
}

/**
 * “Who We Are” page markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {
    content,
    page,
    json,
  } = data

  /** @type {Object} Localized strings, for example, in `_data/en.js` */
  var i18n = data[page.lang]

  return `<!--_layout/who-we-are.11ty.js-->
${content}
<section>
  <header>
    <h2>${i18n.ourMission}</h2>
  </header>
  <p>${i18n.missionStatement}</p>
</section>
<section>
  <header>
    <h2>${i18n.ourVision}</h2>
  </header>
  <p>${i18n.visionStatement}</p>
</section>
<section>
  <header>
    <h2>${i18n.weBelieve}</h2>
  </header>
  <ul>${json.map(p => `<li>${stripHtml(p.content.rendered).result}</li>`).join('\n')}</ul>
</section>`
}
