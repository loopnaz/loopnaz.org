/**
 * @file Donate page layout
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  layout: 'page',
  tags: [
    'primaryNav',
    'getInvolved'
  ],
  templateEngineOverride: '11ty.js,md',
  weight: 5
}

/**
 * Donate page markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {
    content,
    geojson,
    page
  } = data

  /** @type {Object} Localized strings, for example, in `_data/en.js` */
  var i18n = data[page.lang]

  /** @type {Object} Address from GeoJSON data */
  var {address} = geojson.location.properties

  return `<!--_layout/about.11ty.js-->
<script src=https://zeffy-scripts.s3.ca-central-1.amazonaws.com/embed-form-script.min.js></script>
<button class="accent-background glow" zeffy-form-link="https://www.zeffy.com/${page.lang}-US/embed/donation-form/70c0325a-cd0b-4fa9-ae27-c15860caa148?modal=true">${i18n.giveNow}</button>
${content}
<address>
  ${i18n.siteName}<br>
  ${address.street}<br>
  ${address.city}, ${address.state.name} ${address.postalCode}
</address>`
}
