# `_layouts/`

This directory contains Eleventy [layout files](https://www.11ty.dev/docs/layouts/))), which are special templates for wrapping and reusing content. For example:

* base.11ty.js is the outer wrapper for most HTML files
* page.11ty.js [chains](https://www.11ty.dev/docs/layout-chaining/) inside the base layout for pages
* Some pages have a special layout that matches their `page.fileSlug` in the content directories
