/**
 * @file Homepage layout
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  layout: 'base',
  templateEngineOverride: '11ty.js,md',
  eleventyComputed: {
    title: data => data[data.page.lang]?.tagline
  }
}

/**
 * Homepage markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {
    content,
    title
  } = data

  return `<!--_layouts/home.11ty.js-->
<main id="main">
  <section>
    <h1>${title}</h1>
    ${content}
  </section>
</main>`
}
