/**
 * @file Contact page layout
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  layout: 'page',
  tags: 'discover',
  weight: 3,
  templateEngineOverride: '11ty.js,md'
}

/**
 * Contact page markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {
    page,
    content
  } = data

  /** @type {Object} Localized strings, for example, in `_data/en.js` */
  var i18n = data[page.lang]

  return `<!--_layout/contact.11ty.js-->
${content}
<form id="contact"
  class="flex flex-column"
  method="POST"
  action="${this.locale_url('/contact-thanks/')}"
  autocomplete="on"
  netlify-honeypot="bot_field"
  data-netlify="true">
  <label class="screen-reader">${i18n.contact.labels.honeyPot} <input name="bot_field"></label>
  <label for="name">${i18n.contact.labels.name}</label>
  <input id="name"
   name="name"
   type="text" name="name"
   required="true">
  <label for="email">${i18n.contact.labels.email}</label>
  <input id="email"
   name="email"
   type="email"
   required="true">
  <label for="message">${i18n.contact.labels.message}</label>
  <textarea id="message"
    name="message"
    rows="13"
    cols="47"></textarea>
  <button id="submit"
    class="glow"
    name="submit"
    type="submit">${i18n.contact.labels.sendButton}</button>
</form>`
}
