/**
 * @file Base layout
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/ Layouts in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */

// Front matter data
exports.data = {}

/**
 * Base layout markup
 * @async
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = async data => {
  /** @type {Object} Destructure `data` properties used in this template */
  var {
    meta: {baseUrl, copyright, defaultLanguage},
    collections,
    colors,
    content,
    description,
    geojson,
    page,
    tags,
    title,
  } = data

  /** @type {Object} Localized strings, for example, in `_data/en.js` */
  var i18n = data[page.lang]

  /** @type {string} Localized url for the homepage, for example, `/es/` */
  var home = page.lang === defaultLanguage
    ? '/' // Content negotiation on `/` only for the default language
    : this.locale_url('/', page.lang) // Distinct URLs for other translations of the homepage

  /** @type {Object} Address and phone number from GeoJSON data */
  var {address, phone} = geojson.location.properties

  /**
   * Filter 1
   * @param {Object} item description
   * @return {Boolean} true or false
   */
  var inLocale = item => item?.data?.page?.lang === page?.lang

  /**
   * Filter 2
   * @param {Object} item Item description
   * @return {Boolean} True or false
   */
  var inFooter = item => item.data?.inFooter

  /**
   * Function for sorting items by their `weight` property (ascending).
   * @param {Object} a Compare with item b
   * @param {Object} b Compare with item a
   * @return {Object[]} Items
   */
  var byWeight = (a, b) => a.data.weight - b.data.weight

  /**
   * Markup for an item in a navigation list
   * @param {Object} item Destructured
   * @return {string} HTML
   */
  var navItem = ({data: {page, shortTitle, title}}) => `<li><a href="${page.url}">${shortTitle ? shortTitle : title}</a></li>`

  /**
   * Markup for a navigation list
   * @param {Object} collection In Eleventy’s collections object (tags)
   * @param {string} separator Characters to appear between lint items
   * @return {string} HTML
   */
  var navList = (collection, separator = '\n') => {

    return collections[collection]?.filter(inLocale)
      .sort(byWeight)
      .map(navItem)
      .join(separator)
  }

  // Description fallback to localized taglineHow to separate 
  description = description ? description : i18n.tagline

  // Title fallback to `name` from `_data/meta.js`
  title = title ? title : i18n.siteName

  return `<!--_layouts/base.11ty.js-->
<!DOCTYPE html>
<html lang="${page.lang}" dir="${data.dir}">
  <head>
    <title>${title}</title>
    <meta name="description" content="${description}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <!--Font loading first stage-->
    <link rel="preload" href="/fonts/lato/Lato-Bold-kern-latin.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/fonts/lato/Lato-Regular-kern-latin.woff2" as="font" type="font/woff2" crossorigin>

    <!--Font loading second stage-->
    <script async>
      ${await this.minifyJS(this.fileToString('js/fonts.js'))}
    </script>

    <!--Critical CSS-->
    <style>
      :root {
        /* Units */
        --base-unit: 1em;
        --base-unit-half: calc(var(--base-unit) / 2);
        --base-unit-2x: calc(var(--base-unit) * 2);
        --base-unit-3x: calc(var(--base-unit) * 3);
        --full-width: calc(100vw - var(--base-unit-2x));
        --paragraph-width: 66ch;

        /* Colors */
        --black: ${colors.black.hex};
        --gray-100: ${colors.gray[100].hex};
        --gray-300: ${colors.gray[100].hex};
        --gray-500: ${colors.gray[500].hex};
        --gray-700: ${colors.gray[700].hex};
        --red-700: ${colors.red[700].hex};
        --blue-100: ${colors.blue[100].hex};
        --blue-300: ${colors.blue[300].hex};
        --blue-500: ${colors.blue[500].hex};
        --blue-700: ${colors.blue[700].hex};
        --blue-900: ${colors.blue[900].hex};

        /* Variables */
        --text-color: var(--gray-700);
        --background-color: white;
        --accent-color: var(--red-700);
        --border-color: var(--gray-100);
        --border: 1px solid var(--border-color);

        /**
         * Dark mode variants
         * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme MDN}
         */
        @media (prefers-color-scheme: dark) {
          --text-color: white;
          --background-color: var(--black);
          --border-color: var(--gray-500);
        }
      }
      ${this.minifyCSS(`<!--Inline CSS-->
        ${this.fileToString('css/fonts.css')}
        ${this.fileToString('css/layout.css')}
        ${this.fileToString('css/typography.css')}
        ${this.fileToString('css/forms.css')}
        ${this.fileToString('css/screen-reader.css')}
      `)}
    </style>

    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png?v=2.0.0">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png?v=2.0.0">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png?v=2.0.0">
    <link rel="manifest" href="/favicons/site.webmanifest?v=2.0.0">
    <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg?v=2.0.0" color="#c72d27">
    <link rel="shortcut icon" href="/favicons/favicon.ico?v=2.0.0">
    <meta name="apple-mobile-web-app-title" content="LoopNaz">
    <meta name="application-name" content="LoopNaz">
    <meta name="msapplication-TileColor" content="#c72d27">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml?v=2.0.0">
    <meta name="theme-color" content="#ffffff">

    <!--Open Graph-->
    <meta property="og:title" content="${title}">
    <meta property="og:description" content="${description}">
    <meta property="og:url" content="${baseUrl}/index.html">
    <meta property="og:type" content="website">
    <meta property="og:image" content="${baseUrl}/img/og.png">
    <meta property="og:image:alt" content="Logo for the Loop Church of the Nazarene: a red six-pointed star blazened with a flaming dove alighting on an open Bible">

    <!--Twitter-->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@LoopNaz">
    <meta name="twitter:description" content="${description}">
    <meta name="twitter:image" content="${baseUrl}/img/og.png">
  </head>
  <body class="grid" ${tags ? `data-tag="${tags.map(tag => `${tag}`).join(' ')}"` : ''}>
    <a class="screen-reader" href="#main">${i18n.screenReader.skipToMain}</a>
    <header id="site_header">
      ${this.locale_links(page.url).length >= 1
        ? `<nav class="languages-nav emoji-text inline small" aria-labelledby="languages-nav"><h2 aria-labelledby="languages-nav" class="screen-reader">${i18n.screenReader.selectLanguage}</h2><ul class="no-list-style">${this.locale_links(page.url).map(({url, lang, label}) =>
          `<li><a href="${url}" lang="${lang}" hreflang="${lang}">${label} (${i18n.languages[lang]})</a></li>`
        ).join(', ')}</ul></nav>`
        : ''}
      <nav id="primary_nav" aria-label="primary" class="flex">
        <a href="${home}"><span class="screen-reader">${i18n.screenReader.homepage}</span><svg width="405" height="96"><use href="/${page.lang}/icons.svg#logo-color"></use></svg></a>
        <ul class="flex gap no-list-style bold uppercase">
          ${navList('primaryNav')}
        </ul>
      </nav>
    </header>
    ${content}
    <footer id="site_footer" class="grid">
      <nav id="footer_nav" aria-label="footer" class="grid gap columns">
        <section id="contact_info">
          <a href="${home}"><span class="screen-reader">${i18n.screenReader.homepage}</span><svg width="270" height="64"><use href="/${page.lang}/icons.svg#logo-color"></use></svg></a>
          <ul class="no-list-style inline social">
            ${data.social.map(({name, icon, url}) => `<li><a href="${url}"><span class="screen-reader">${name}</span><svg height="16"><use href="/img/social.svg#${icon}"></use></svg></a></li>`).join('')}
          </ul>
          <address>
            <p>${address.street}<br>
              ${address.city}, ${address.state.name}&nbsp;${address.postalCode}
            </p>
            <p><a href="tel:${phone.tel}">${phone.formatted}</a></p>
          </address>
        </section>
        <section id="join_us">
          <h2>${i18n.joinUs}</h2>
          <ul class="no-list-style">
            ${collections.services
              ?.filter(inLocale)
              .sort(byWeight)
              .map(({data: {page, shortTitle, serviceTime, title}}) => 
                `<li><a href="${page.url}">${shortTitle ? shortTitle : title}</a>${serviceTime ? `&nbsp;<!--serviceTime--><time>${serviceTime}</time>` : '<!--no serviceTime-->'}</li>`
              ).join('\n')}
          </ul>
        </section>
        <section id="discover">
          <h2>${i18n.discover}</h2>
          <ul class="no-list-style">
            ${navList('discover')}
          </ul>
        </section>
        <section>
          <h2>${i18n.getInvolved}</h2>
          <ul class="no-list-style">
            ${navList('getInvolved')}
          </ul>
        </section>
        <section id="learn_more">
          <h2>${i18n.learnMore}</h2>
          <ul class="no-list-style">
            ${navList('learnMore')}
          </ul>
        </section>
      </nav>
      <nav id="site_policies" class="inline small" aria-labelledby="footer_policies">&copy; ${copyright} ${i18n.siteName} | <span id="footer_policies" class="screen-reader">${i18n.screenReader.policies}</span><ul class="no-list-style">${collections.policies
        ?.filter(item => inLocale(item) && inFooter(item))
        .sort(byWeight)
        .map(navItem)
        .join('<span aria-hidden="true"> | </span>')}</ul></nav>
    </footer>
    <script async>
      ${await this.minifyJS(this.fileToString('js/register-service-worker.js'))}
    </script>
  </body>
</html>`
}
