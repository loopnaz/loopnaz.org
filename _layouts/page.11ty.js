/**
 * @file Page layout
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/ Layouts in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */

// Front matter data
exports.data = {
  layout: 'base'
}

/**
 * Page markup
 * @async
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this template */
  var {
    content,
    description,
    title
  } = data

  // Title fallback to `name` from `_data/meta.js`
  return `<!--_layouts/page.11ty.js-->
<main id="main">
  <article>
    <header>
      <h1>${title}</h1>
      ${description
        ? `<!--Description-->
          <p>${description}</p>`
        : '<!--No description-->'}
    </header>
    ${content}
  </article>
</main>`
}
