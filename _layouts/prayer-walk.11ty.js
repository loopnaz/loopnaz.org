/**
 * @file Prayer Walk page layout
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  layout: 'page',
  tags: 'services',
  templateEngineOverride: '11ty.js,md',
  weight: 2
}

/**
 * Prayer Walk page markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {content} = data

  return `<!--_layouts/prayer-walk.11ty.js-->
${content}`
}
