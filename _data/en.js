/**
 * @file Global data for English strings
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global Data Files in Eleventy}
 */

/**
 * Define global data for English strings
 * @module _data/en
 */
module.exports = {
  // Language names in English
  languages: {
    es: 'Spanish'
  },
  /* Identity */

  // The name of the site
  siteName: 'Church of the Nazarene in the Loop',
  // The site tagline
  tagline: 'Let’s Serve the Heart of Chicago',

  /* User interface */

  // Screen reader text
  screenReader: {
    homepage: 'Go to homepage',
    policies: 'Policies',
    skipToMain: 'Skip to main content',
    // Language switcher
    selectLanguage: 'Choose another language'
  },
  // Donate page
  giveNow: 'Give Now',
  // “Contact us” invitation
  contact: {
    invitation: 'Contact us',
    labels: {
      honeyPot: 'Skip this field if you are a human',
      name: 'Name',
      email: 'Email',
      message: 'Message',
      sendButton: 'Send'
    }
  },

  /* Footer navigation headers */

  // Join Us
  joinUs: 'Join Us',
  // Discover
  discover: 'Discover',
  // Get Involved
  getInvolved: 'Get Involved',
  // Learn More
  learnMore: 'Learn More',

  /* Agreed Statement of Belief */
  beliefs: [
    {
      content: {
        rendered: 'In one God—the Father, Son, and Holy Spirit.'
      }
    },
    {
      content: {
        rendered: 'The Old and New Testament Scriptures, given by plenary inspiration, contain all truth necessary to faith and Christian living.'
      }
    },
    {
      content: {
        rendered: 'Human beings are born with a fallen nature, and are, therefore, inclined to evil, and that continually.'
      }
    },
    {
      content: {
        rendered: 'The finally impenitent are hopelessly and eternally lost.'
      }
    },
    {
      content: {
        rendered: 'The atonement through Jesus Christ is for the whole human race; and that whosoever repents and believes on the Lord Jesus Christ is justified and regenerated and saved from the dominion of sin.'
      }
    },
    {
      content: {
        rendered: 'That believers are to be sanctified wholly, subsequent to regeneration, through faith in the Lord Jesus Christ.'
      }
    },
    {
      content: {
        rendered: 'The Holy Spirit bears witness to the new birth, and also to the entire sanctification of believers.'
      }
    },
    {
      content: {
        rendered: 'Our Lord will return, the dead will be raised, and the final judgment will take place.'
      }
    }
  ]
}
