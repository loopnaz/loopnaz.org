/**
 * @file Datos globales españoles
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Archivos de datos globales en Eleventy}
 */

/**
 * Datos globales españoles
 * @module _data/es
 */
module.exports = {
  // Nombres de idiomas en español
  languages: {
    en: 'Inglés'
  },
  /* Identidad */

  // El nombre del sitio
  siteName: 'Iglesia del Nazareno en el Loop',
  // La lema del sitio
  tagline: 'Sirvamos al Corazón de Chicago',

  /* Interfaz de usuario */

  // Texto del lector de pantalla
  screenReader: {
    homepage: 'Ir a la página principal',
    policies: 'Políticas',
    skipToMain: 'Saltar al contenido principal',
    // Language switcher
    selectLanguage: 'Elige otro idioma'
  },
  // Página de donaciónes
  giveNow: 'Dar ahora',
  // Invitacion a contáctenos
  contact: {
    invitation: 'Contáctenos',
    labels: {
      honeyPot: 'Omita esta entrada si es un humano',
      name: 'Nombre',
      email: 'Correo electrónico',
      message: 'Mensaje',
      sendButton: 'Enviar'
    }
  },

  /* Encabezados de navegación de pie de página */

  // Únete a nosotros
  joinUs: 'Únete a nosotros',
  // Descubre
  discover: 'Descubre',
  // Involúcrese
  getInvolved: 'Involúcrese',
  // Aprende más
  learnMore: 'Aprende más',

  /* Declaración convenida de fe */
  beliefs: [
    {
      content: {
        rendered: 'En un solo Dios — Padre, Hijo y Espíritu Santo.'
      }
    },
    {
      content: {
        rendered: 'Las Escrituras del Antiguo y Nuevo Testamento, dadas por inspiración plenaria, contienen toda la verdad necesaria para la fe y la vida cristiana.'
      }
    },
    {
      content: {
        rendered: 'El ser humano nace con una naturaleza caída y, por lo tanto, está inclinado al mal continuamente.'
      }
    },
    {
      content: {
        rendered: 'Los que permanecen impenitentes hasta el fin están perdidos eternamente y sin esperanza.'
      }
    },
    {
      content: {
        rendered: 'La expiación por medio de Jesucristo es para toda la raza humana; y que todo aquel que se arrepiente y cree en el Señor Jesucristo es justificado, regenerado y salvado del dominio del pecado.'
      }
    },
    {
      content: {
        rendered: 'Que los creyentes han de ser enteramente santificados, subsecuente a la regeneración, por medio de la fe en el Señor Jesucristo.'
      }
    },
    {
      content: {
        rendered: 'El Espíritu Santo da testimonio del nuevo nacimiento y también de la entera santificación de los creyentes.'
      }
    },
    {
      content: {
        rendered: 'Nuestro Señor regresará, que los muertos resucitarán y que se llevará a cabo el juicio final.'
      }
    }
  ]
}
