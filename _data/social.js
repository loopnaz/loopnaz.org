/**
 * @file Global data for social media accounts
 * @author Reuben L. Lillie
 */

// Import
var pkg = require('./../package')

/**
 * Social media global data
 *
 * @module _data/social
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */
module.exports = [
  {
    name: 'Facebook',
    icon: 'facebook',
    url: 'https://facebook.com/loopnaz/'
  },
  {
    name: 'Instagram',
    icon: 'instagram',
    url: 'https://instagram.com/loopnaz/'
  },
  {
    name: 'LinkedIn',
    icon: 'linkedin',
    url: 'https://www.linkedin.com/company/loopnaz/'
  },
  {
    name: 'Messenger',
    icon: 'messenger',
    url: 'https://m.me/loopnaz/'
  },
  {
    name: 'Git',
    icon: 'git',
    url: `${pkg.repository.home}`
  },
]
