/**
 * @file Global metadata
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/data-global/ Global Data Files in Eleventy}
 */

/**
 * Define global metadata
 * @module _data/meta
 */
module.exports = {
  // The base URL for the site, but without a trailing slash
  baseUrl: 'https://loopnaz.org',
  // Copyright year
  copyright: new Date().getFullYear(),
  // Default language
  defaultLanguage: 'en'
}
