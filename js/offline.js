/**
 * @file Defines a client-side script for offline visits to pages not `CacheStorage`
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://github.com/cferdinandi/go-make-things/blob/master/content/offline.md Example offline landing page by Chris Ferdinandi}
 */

// Immediately invoked function expression to enclose scope
(async () => {
  /**
   * Define markup for listing cached pages
   * @param {Promise.<Request[]>} pages `Request` objects in pages cache
   * @return {String} HTML
   */
  var html = pages => {
    /** @type  {Object} DOM object with `data-offline attribute */
    var offline = document.querySelector('[data-offline]');

    // Only create the markup when there are pages to list
    offline.innerHTML = `<!--./js/offline.js-->
      <p>You can still access these pages you’ve already viewed:</p>
      <ul>
        ${pages.map(page => `<li>
          <a href="${page.url}">${page.url}</a>
        </li>`).join('\n')}
      </ul>`
    return
  }

  /**
   * Display pages from the `CacheStorage` API
   * @async
   * @param {String} key `cacheName` to request from `CacheStorage`
   * @return {Function} cachedPages
   */
  async function displayCachedPages(key) {
    /** @type {Promise.<Cache>} `Cache` obejct matching the `key` paramater */
    var cache = await caches.open(key)
    /** @type {Promise.<Request[]>} `Request` objects */

    var keys = await cache.keys()
    /** @type {Request[]} `Request` objects that are pages, not including the offline landing page */
    var pages = keys.filter(key => key.url.endsWith('/') && !key.url.includes('/offline/'))

    return pages.length > 0
      ? html(pages)
      : console.warn('No pages to display in CacheStorage')
  }

  /**
   * Get pages from the `CacheStorage` API
   * @async
   * @return {string[]} From the page cache
   */
  async function getCachedPages() {
    /** @type {Promise.<string[]>} Names of `Cache` objects */
    var keys = await caches.keys()
    return keys.filter(key => key.includes('_pages'))
  }

  // Client supports service workers
  if(navigator && navigator.serviceWorker) {
    var cachedPages = await getCachedPages()

    return cachedPages.forEach(key => displayCachedPages(key))
  }

  return
})()
