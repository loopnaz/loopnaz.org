/**
 * @file Register the app service worker
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://gomakethings.com/writing-your-first-service-worker-with-vanilla-js/ “Writing Your First Service Worker in Vanilla JS” by Chris Ferdinandi}
 */

(() => {
  /**
   * Register the service worker
   * @return {void}
   */
  async function registerServiceWorker() {
    if ('serviceWorker' in navigator) {
      try {
        await navigator.serviceWorker.register('/service-worker.js', {
          scope: '/',
        })
      } catch (error) {
        console.error(`Service worker registration error: ${error}`)
      }
    }
  }

  registerServiceWorker()

  return
})()
