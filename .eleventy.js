/**
 * @file Configures preferences for Eleventy
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @see {@link https://www.11ty.dev/docs/config/ Eleventy Documentation}
 */

// Import Eleventy plugins
var {EleventyI18nPlugin} = require('@11ty/eleventy')

var {defaultLanguage} = require('./_data/meta')

// Import filters, shortcodes, and transforms from their directories
var filters = require('./_includes/filters')
var shortcodes = require('./_includes/shortcodes')
var transforms = require('./_includes/transforms')

/**
 * Eleventy’s configuration module
 * @module .eleventy
 * @param {object} eleventyConfig Eleventy’s Config API
 * @return {object} {} Eleventy’s optional Config object
 * @see {@link https://www.11ty.dev/docs/config/}
 */
module.exports = eleventyConfig => {

  /*
   * Filters, shortcodes, and transforms
   * Added anonymously instead of as arrow functions to leverage Eleventy supplied data with the keyord `this`
   */

  // Create an array of all the filter names
  Object.keys(filters).forEach(filter =>
    // Configure each filter with Eleventy
    eleventyConfig.addFilter(filter, filters[filter]))

  // Create an array of all the shortcode names
  Object.keys(shortcodes).forEach(shortcode =>
    // Configure each shortcode with Eleventy
    eleventyConfig.addShortcode(shortcode, shortcodes[shortcode]))

  // Create an array of all the transform names
  Object.keys(transforms).forEach(transform =>
    // Configure each transform with Eleventy
    eleventyConfig.addTransform(transform, transforms[transform]))

  /*
   * Plugins
   */

  /*
   * Configure Eleventy’s internationalization (i18n) plugin
   * @see {@link https://www.11ty.dev/docs/plugins/i18n/ Internationalization (i18n) in Eleventy}
   */
  eleventyConfig.addPlugin(EleventyI18nPlugin, {
    defaultLanguage: defaultLanguage, // Required
  })

  /*
   * Copy static assets to the output directory
   * @see {@link https://www.11ty.dev/docs/copy/ Passthrough file copy in Eleventy}
   */
  eleventyConfig.addPassthroughCopy('favicons')
  eleventyConfig.addPassthroughCopy('fonts')
  eleventyConfig.addPassthroughCopy('img/icons')
  eleventyConfig.addPassthroughCopy('service-worker.js')

  return {
    dir: {
      /**
       * @see {@link https://www.11ty.dev/docs/config/#directory-for-layouts-(optional) Directory for Layouts in Eleventy}
       */
      layouts: '_layouts'
    }
  }
}
