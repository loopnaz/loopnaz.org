/**
 * @file “Who We Are” page template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

var EleventyFetch = require('@11ty/eleventy-fetch')

/**
 * Front matter data
 */
exports.data = {
  title: 'Who We Are',
  layout: 'who-we-are',
  en: {
    weBelieve: 'We Believe',
    ourMission: 'Our Mission',
    missionStatement: 'To make Christlike disciples in the nations',
    ourVision: 'Our Vision',
    visionStatement: 'To form a network of compassionate churches in every neighborhood of every major city around the world'
  },
  beliefsUrl: 'https://2017.manual.nazarene.org/wp-json/wp/v2/paragraphs?parent=101&order=asc&orderby=menu_order',
  templateEngineOverride: '11ty.js,md'
}

/**
 * “Who We Are” page markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {
    page,
    content
  } = data

  return `<!--${page.inputPath}-->
We are a church in Chicago’s Loop driven by compassion for our neighbors in greatest need.

Following the example of Jesus of Nazareth, we are here to seek and to serve the toiling, struggling, sorrowing heart of the world.

We also belong to a global family. The Church of the Nazarene is home to over 2.6 million members worshiping in more than 30,000 congregations in 164 world areas.`
}
