/**
 * @file Directory data common to all pages
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in Eleventy}
 */

/**
 * Directory data
 * @module content/en/pages
 */
module.exports = {
  /** @see {@link https://www.11ty.dev/docs/collections/ Collections in Eleventy} */
  tags: 'pages',
  /** @see {@link https://www.11ty.dev/docs/layouts/ Layouts in Eleventy} */
  layout: 'page',
  /** @see {@link https://www.11ty.dev/docs/data-computed/ Computed data in Eleventy} */
  eleventyComputed: {
    /**
     * Format permlinks as `/page.fileSlug/`
     * @see {@link https://www.11ty.dev/docs/permalinks/ Permalinks in Eleventy} 
     */
    permalink: data => `/${data.page.fileSlug}/index.html`
  }
}
