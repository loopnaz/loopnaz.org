/**
 * @file Donate page template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  title: 'Donate',
  layout: 'donate',
  templateEngineOverride: '11ty.js,md'
}

/**
 * Donate page markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {page} = data

  /** @type {Object} Localized strings, for example, in `_data/en.js` */
  var i18n = data[page.lang]

  return `<!--${page.inputPath}-->
> We love because God first loved us.

> —1 John 4:19 NIV

### Make a Difference

When you donate to the Church of the Nazarene in the Loop, you make a lasting difference.

Your gift helps us clothe, shelter, feed, heel, educate, and live in solidarity with our neighbors who are suffering under oppression, injustice, violence, poverty, hunger, and disease.

Thank you for serving the heart of Chicago with us.

### Giving Online

Our online giving platform, [Zeffy](https://www.zeffy.com/en-US/donation-form/70c0325a-cd0b-4fa9-ae27-c15860caa148), does not charge processing fees. That means 100% of your donation comes to us.

Instead, please be aware that Zeffy will automatically ask you for an additional donation, usually between 10% and 15%. You can reset that amount to $0 or another amount you wish. We do not expect you to donate to Zeffy when you donate to us.

### Cash or Check

Print, complete, and mail your form to:`
}
