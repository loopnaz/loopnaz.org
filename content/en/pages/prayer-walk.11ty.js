/**
 * @file Prayer Walk page template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  title: 'Daily Prayer Walk',
  layout: 'prayer-walk',
  templateEngineOverride: '11ty.js,md'
}

/**
 * Prayer Walk page markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {page} = data

  return `<!--${page.inputPath}-->
Daily Prayer Walk`
}
