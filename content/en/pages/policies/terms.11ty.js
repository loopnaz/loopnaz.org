/**
 * @file Terms of use template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  title: 'Terms of Use',
  shortTitle: 'Terms',
  inFooter: true,
  weight: 2,
  templateEngineOverride: '11ty.js,md',
}

/**
 * Terms of use markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this template */
  var {page} = data

  return `<!--${page.inputPath}-->
Thank you for visiting our site.

We want you to enjoy using our site and feel safe at the same time. Here’s what you need to know:

## About these terms

These terms explain the relationship between you and us as the owners and developers of this site. These terms don’t give any special rights to third parties. We might update these terms. If you disagree with those changes, then you should stop using our site.

## Accessibility

We detail the standards and processes we follow to ensure that our website is usable by everyone, particularly those with disabilities, in our [accessibility statement](/accessibility/).

## Privacy

We respect your privacy and explain how we handle your personal information in our [privacy policy](/privacy/).

## Copyright

We also clarify what you can and cannot do with the content on our site and its source code in our [copyright notice](/copyright/).

## Warranties

Our site is provided “as is.” We don’t make any special promises about it. We don’t guarantee that it’s suitable for any particular purpose. And we’re not responsible for any issues you might encounter.

## Liability

We won’t be responsible for any unforeseeable losses or damages.

## Disputes

If there’s a conflict between these terms and any additional terms specific to a certain part of our site, those additional terms will be take precedence. If you don’t follow these terms, and we don’t take action immediately, then that doesn’t mean we’re giving up our rights.

## Governing law

These terms are governed by the laws of the State of Illinois in the United States of America. If there’s a legal dispute, then you and we agree it will be handled in the federal or state courts of Cook County, Illinois.`
}
