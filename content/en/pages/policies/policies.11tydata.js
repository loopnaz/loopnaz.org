/**
 * @file Directory data common to all policy pages
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in Eleventy}
 */

/**
 * Directory data
 * @module content/en/pages/policies
 */
module.exports = {
  /**
   * Names of the collections
   * @see {@link https://www.11ty.dev/docs/collections/ Collection in Eleventy}
   */
  tags: 'policies',
  /**
   * Content date
   * @see {@link https://www.11ty.dev/docs/dates/ Content dates in Eleventy}
   */
  date: 'Last Modified'
}
