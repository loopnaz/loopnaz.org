/**
 * @file Privacy policy template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  title: 'Privacy Policy',
  shortTitle: 'Privacy',
  inFooter: true,
  weight: 1,
  templateEngineOverride: '11ty.js,md',
}

/**
 * Privacy policy markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this template */
  var {page} = data

  return `<!--${page.inputPath}-->
## What information do our sites collect?

Our sites do not gather any personal details unless you willingly provide them to us.

For example, when you fill out our [contact form](/contact/), we ask you for two pieces of personal information:

1. Your name; and
1. A valid email address you will use to communicate with us.

If you do not tell us you visited our site, then we will not know anything about you or your visit.

Services like our online giving processor have their own privacy policies, which to our knowledge meet or exceed international privacy standards.

## How do we use the information we collect through our sites?

We only use the personal information you share with us to communicate with you directly.

## What information do our sites share?

Whenever you share personal information with us on our sites, we will not share it with anyone unless you know about it and give us your explicit permission.

## What if we want to share the information collected through our sites?

If we ever need to share the personal information you have given us on our sites, we will first ask for your permission. We will reach out to you by email, phone, social media, or another communication method, and ask you if we can share specific details. We will only share it after you give us permission.

## Changes to this policy

We might update this privacy policy occasionally, but we will not diminish your rights without your consent. We will always show the date of the most recent changes with links to previous versions. If we make significant changes, then we will notify you more prominently.`
}
