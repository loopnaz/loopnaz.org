/**
 * @file Accessibility Statement template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  title: 'Accessibility Statement',
  templateEngineOverride: '11ty.js,md',
}

/**
 * Accessibility Statement markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {

  /** @type {Object} Destructure `data` properties used in this template */
  var {
    page,
    pkg: {repository},
  } = data

  return `<!--${page.inputPath}-->
We’re committed to making my site accessible for everyone, especially if you have a disability.

## Minimum standards

Our site meets or exceeds [Web Content Accessibility Guidelines 2.1][wcag2aa] Level AA.

## Additional considerations

We also take the following measures to ensure our site remains accessible:

* Use clear written lanuage
* Make the user interface easy to understand
* Run automated and manual tests for accessibility
* Honor your system preferences
* Limit file sizes and device memory loads
* Publish our source code in a [public repository][git], allowing anyone to inspect it and suggest changes
* Document our code, to be as transparent as possible—even if you have little or no programming experience

[git]: ${repository.home}
[wcag2aa]: https://www.w3.org/WAI/standards-guidelines/wcag/`
}
