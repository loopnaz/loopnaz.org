/**
 * @file Copyright notice template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Front matter data
 */
exports.data = {
  title: 'Copyright Notice',
  templateEngineOverride: '11ty.js,md',
}

/**
 * Copyright notice markup
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this template */
  var {
    page,
    pkg: {repository},
  } = data

  return `<!--${page.inputPath}-->
We value creativity, and we want you to feel free to improve upon our work.

## Copyrighted content

Unless otherwise noted, content on this site is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International license][cc-by-sa].

You are free to copy and redistribute the material in any medium or format. You are also free to remix, transform, and build upon the material for any purpose, even commercially. We will not revoke these terms as long as you follow the terms of this license.

You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests we endorse you or your use.

If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

For further permissions, please [contact us][contact].

## Source code license

The source code for this site is licensed under the [MIT License][mit]. A copy of the license is available in the source code repository on [GitLab][mit].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[contact]: /contact/
[mit]: https://gitlab.com/reubenlillie/reubenlillie.com/blob/master/LICENSE/`
}
