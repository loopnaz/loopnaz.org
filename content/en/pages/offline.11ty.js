/**
 * @file Offline landing page template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */

// Front matter data
exports.data = {
  title: 'Your device is offline',
  description: 'This page appears when your device is offline. Use these links to revisit pages you’ve browsed until  you get back online.'
}

/**
 * Offline markup
 * @async
 * @return {string} The rendered template
 */
exports.render = async data => {
  /** @type {Object} Destructure `data` properties used in this template */
  var {page} = data

  return `<!--${page.inputPath}-->
<div data-offline></div>
<script>
  ${await this.minifyJS(this.fileToString('js/offline.js'))}
</script>`
}
