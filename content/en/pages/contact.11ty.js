/**
 * @file Contact page template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */

/**
 * Front matter data
 */
exports.data = {
  title: 'Contact Us',
  description: 'Thanks for contacting us!',
  layout: 'contact'
}

/**
 * Contact page markup
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered template
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this template */
  var {page} = data

  return `<!--${page.inputPath}-->
Contact`
}

