/**
 * @file 404 page template
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 * @see {@link https://www.11ty.dev/docs/quicktips/not-found/ 404 pages in Eleventy}
 */

/*
 * Front matter data
 */
exports.data ={
  en: {
    title: 'Don’t see what you need?'
  },
  es: {
    title: '¿No ves lo que necesitas?'
  },
  layout: 'page',
  permalink: '404.html',
  templateEngineOverride: '11ty.js,md',
  eleventyExcludeFromCollections: true
}

/**
 * 404 page markup
 * @param {Object} data Eleventy’s `data` object (destructured)
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this template */
  var {page} = data

  /** @type {Object} Localized strings, for example, in `_data/en.js` */
  var i18n = data[page.lang]

  return `<!--404.11ty.js-->
[${i18n.contact}](${this.locale_url('/', 'es')})`
}
