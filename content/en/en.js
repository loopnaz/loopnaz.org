/**
 * @file Directory data common to all content in English
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in Eleventy}
 */

/**
 * Directory data
 * @module content/en
 */
module.exports = {
  /**
   * IETF BCP 47 language tag
   * @see {@link https://r12a.github.io/app-subtags/ BCP47 language subtag lookup}
   * */
  lang: 'en',
  /**
   * Text direction
   * @see {@link https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/dir MDN}
   */
  dir: 'ltr'
}
