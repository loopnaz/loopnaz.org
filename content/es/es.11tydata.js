/**
 * @file Datos de directorio comunes a todo el contenido en español
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Datos de templatos y directorios en Eleventy}
 */

/**
 * Datos de directorio
 * @module content/es/pages
 */
module.exports = {
  /**
   * Código de idioma IETF
   * @see {@link https://r12a.github.io/app-subtags/ Búsqueda de subetiquetas de idioma BCP47}
   * */
  lang: 'es',
  /**
   * Direccionalidad del texto
   * @see {@link https://developer.mozilla.org/es-US/docs/Web/HTML/Global_attributes/dir MDN}
   */
  dir: 'ltr'
}
