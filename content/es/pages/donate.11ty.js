/**
 * @file La pantilla para la página de donaciones
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Encadenamiento de diseños en Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method `data` opcionales de plantillas de JavaScript en Eleventy}
 */

/**
 * Datos preliminares
 */
exports.data = {
  title: 'Donar',
  layout: 'donate',
  slug: 'donar',
  templateEngineOverride: '11ty.js,md'
}

/**
 * El marcado de la página de donaciones
 * @param {Object} data El objeto de `data` en Eleventy
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructurar propriedades de `data` usado en esta pantilla */
  var {page} = data

  /** @type {Object} Cadenas localizadas, por ejemplo, en `_data/es.js` */
  var i18n = data[page.lang]

  return `<!--${page.inputPath}-->
> Nosotros amamos porque él nos amó primero.

> —1 Juan 4:19 NVI

### Hacer la diferencia

Quando hace una donación a la Iglesia del Nazareno en el Loop, marcas una diferencia duradera.

Tu donación nos ayuda a vestir, albergar, almentar, educar y vivir en solidaridad con nuestros vecinos que sufren bajo opresión, injusticia, violencia, pobreza, hambre y enfermedades.

Gracias por servir al corazón de Chicago con nosotros.

### Dar en linea

Nuestra platforma de donaciones en linea, [Zeffy](https://www.zeffy.com/es-US/donation-form/70c0325a-cd0b-4fa9-ae27-c15860caa148), non cobra tarifas de procesamiento. Eso significa que el 100% de tu donación llega a nosotros.

En su lugar, tenga en cuenta que Zeffy le solicitará automáticamente una donación adicional, normalmente entre el 10% y el 15%. Puede restablecer esa cantidad a $0 u otra cantidad que desee. No esperamos que usted haga una donación a Zeffy cuando nos haga una donación a nosotros.

### Efectivo o cheque

Imprima, complete y envíe su formulario por correo a:`
}
