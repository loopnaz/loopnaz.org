/**
 * @file La pantilla de la página de “Que hacemos”
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

/**
 * Datos preliminares
 */
exports.data = {
  title: 'Lo que hacemos',
  layout: 'what-we-do',
  slug: 'lo-que-hacemos',
  templateEngineOverride: '11ty.js,md'
}

/**
 * El marcado de la página de “Que hacemos”
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {page} = data

  return `<!--${page.inputPath}-->
Español`
}
