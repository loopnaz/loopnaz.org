/**
 * @file La pantilla para la página de contacto
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Encadenamiento de diseños en Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method `data` opcionales de plantillas de JavaScript en Eleventy}
 */

/**
 * Datos preliminares
 */
exports.data = {
  title: 'Contáctanos',
  description: '¡Gracias por ponerse en contacto con nosotros!',
  layout: 'contact',
  slug: 'contacto',
  templateEngineOverride: '11ty.js,md'
}

/**
 * El marcado de la página de contacto
 * @param {Object} data El objeto de `data` en Eleventy
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructurar propriedades de `data` usado en esta pantilla */
  var {page} = data

  return `<!--${page.inputPath}-->
Español`
}
