/**
 * @file La pantilla de la página de los derechos de autor
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Encadenamiento de diseños en Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method `data` opcionales de plantillas de JavaScript en Eleventy}
 */

/**
 * Datos preliminares
 */
exports.data = {
  title: 'Derechos de autor',
  slug: 'derechos-de-autor',
  templateEngineOverride: '11ty.js,md',
}

/**
 * El marcado de la página de los derechos de autor
 * @param {Object} data El objeto de `data` en Eleventy
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this template */
  var {
    page,
    pkg: {repository},
  } = data

  return `<!--${page.inputPath}-->
Valoramos la creatividad y queremos que te sientas libre para mejorar nuestro trabajo.

## Contenido con derechos de autor

A menos que se indique lo contrario, el contenido en este sitio está bajo licencia [Creative Commons Atribución-CompartirIgual 4.0 Internacional][cc-by-sa].

Eres libre de copiar y redistribuir el material en cualquier medio o formato. También eres libre de remezclar, transformar y construir sobre el material con cualquier propósito, incluso comercialmente. No revocaremos estos términos siempre y cuando sigas los términos de esta licencia.

Debes dar crédito apropiado, proporcionar un enlace a la licencia e indicar si se realizaron cambios. Puedes hacerlo de cualquier manera razonable, pero no de ninguna manera que sugiera que te respaldamos a ti o a tu uso.

Si remezclas, transformas o construyes sobre el material, debes distribuir tus contribuciones bajo la misma licencia que el original.

No puedes aplicar términos legales o medidas tecnológicas que restrinjan legalmente a otros de hacer cualquier cosa que la licencia permita.

Para obtener permisos adicionales, por favor [contáctanos][contact].

## Licencia del código fuente

El código fuente de este sitio está bajo [licencia MIT][mit]. Una copia de la licencia está disponible en el repositorio de código fuente en [GitLab][mit].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/deed.${page.lang}
[contact]: ${this.locale_url('/contact/')}
[mit]: https://gitlab.com/reubenlillie/reubenlillie.com/blob/master/LICENSE/`
}
