/**
 * @file Datos de directorio comunes a todas las páginas de políticas
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Datos de templatos y directorios en Eleventy}
 */

/**
 * Datos de directorio
 * @module content/es/pages/policies
 */
module.exports = {
  /**
   * Nombres de colecciones en ingles
   * @see {@link https://www.11ty.dev/docs/collections/ Colecciones en Eleventy}
   */
  tags: 'policies',
  /**
   * Fechas de contenido
   * @see {@link https://www.11ty.dev/docs/dates/ Fechas de contenido en Eleventy}
   */
  date: 'Last Modified'
}
