/**
 * @file La pantilla para la página de la condiciones de uso
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Encadenamiento de diseños en Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method `data` opcionales de plantillas de JavaScript en Eleventy}
 */

/**
 * Datos preliminares
 */
exports.data = {
  title: 'Condiciones de uso',
  shortTitle: 'Condiciones',
  slug: 'condiciones',
  inFooter: true,
  weight: 2,
  templateEngineOverride: '11ty.js,md',
}

/**
 * El marcado de la página de la política de privacidad
 * @param {Object} data El objeto de `data` en Eleventy
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructurar propriedades de `data` usado en esta pantilla */
  var {page} = data

  return `<!--${page.inputPath}-->
Gracias por visitar nuestro sitio.

Queremos que disfrutes usando nuestro sitio y te sientas seguro al mismo tiempo. Esto es lo que debes saber:

## Sobre de estos términos

Estos términos explican la relación entre tú y nosotros, como los propietarios y desarrolladores de este sitio. Estos términos no otorgan derechos especiales a terceros. Podemos actualizar estos términos. Si no estás de acuerdo con esos cambios, entonces debes dejar de usar nuestro sitio.

## Accesibilidad

Detallamos los estándares y procesos que seguimos para asegurar que nuestro sitio web sea utilizable por todos, especialmente por aquellos con discapacidades, en nuestra [declaración de accesibilidad](${this.locale_url('/accessibility/')}).

## Privacidad

Respetamos tu privacidad y explicamos cómo manejamos tu información personal en nuestra [política de privacidad](${this.locale_url('/privacy/')}).

## Derechos de autor

También aclaramos lo que puedes y no puedes hacer con el contenido de nuestro sitio y su código fuente en nuestro [aviso de derechos de autor](${this.locale_url('/copyright/')}).

## Garantías

Nuestro sitio se proporciona "tal como está". No hacemos promesas especiales al respecto. No garantizamos que sea adecuado para un propósito particular. Y no somos responsables de los problemas que puedas encontrar.

## Responsabilidad

No seremos responsables de pérdidas o daños imprevisibles.

## Conflictos

Si hay un conflicto entre estos términos y cualquier término adicional específico para una parte particular de nuestro sitio, esos términos adicionales tendrán prioridad. Si no cumples con estos términos y no tomamos medidas de inmediato, eso no significa que renunciemos a nuestros derechos.

## Ley aplicable

Estos términos están sujetos a las leyes del Estado de Illinois en los Estados Unidos de América. Si hay una disputa legal, tú y nosotros acordamos que se resolverá en los tribunales federales o estatales del Condado de Cook, Illinois.`
}
