/**
 * @file Diseño para el página de la política de privacidad
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Encadenamiento de diseños en Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method `data` opcionales de plantillas de JavaScript en Eleventy}
 */

/**
 * Datos preliminares
 */
exports.data = {
  title: 'Política de privacidad',
  shortTitle: 'Privacidad',
  slug: 'privacidad',
  inFooter: true,
  weight: 1,
  templateEngineOverride: '11ty.js,md',
}

/**
 * Margen de la página de la política de privacidad
 * @param {Object} data El objeto de `data` en Eleventy
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructurar propriedades de `data` usado en esta pantilla */
  var {page} = data

  return `<!--${page.inputPath}-->
## ¿Qué información recopilan nuestros sitios?

Nuestros sitios no recopilan ningún detalle personal a menos que los proporcione voluntariamente.

Por ejemplo, cuando completa nuestro [formulario de contacto](${this.locale_url('/contact/')}), le pedimos dos datos personales:

1. Su nombre; y
1. Una dirección de correo electrónico válida que utilizará para comunicarse con nosotros.

Si no nos informa de su visita a nuestro sitio, no sabremos nada acerca de usted ni de su visita.

Los servicios como nuestro procesador de donaciones en línea tienen sus propias políticas de privacidad, que hasta donde sabemos cumplen o superan los estándares internacionales de privacidad.

## ¿Cómo utilizamos la información que recopilamos a través de nuestros sitios?

Solo utilizamos la información personal que comparte con nosotros para comunicarnos directamente con usted.

## ¿Qué información comparten nuestros sitios?

Cada vez que comparte información personal con nosotros en nuestros sitios, no la compartiremos con nadie a menos que usted lo sepa y nos otorgue su permiso explícito.

## ¿Qué sucede si deseamos compartir la información recopilada a través de nuestros sitios?

Si en algún momento necesitamos compartir la información personal que nos ha proporcionado en nuestros sitios, primero le pediremos su permiso. Nos pondremos en contacto con usted por correo electrónico, teléfono, redes sociales u otro medio de comunicación y le preguntaremos si podemos compartir detalles específicos. Solo lo compartiremos después de que nos otorgue su permiso.

## Cambios en esta política

Es posible que actualicemos esta política de privacidad ocasionalmente, pero no reduciremos sus derechos sin su consentimiento. Siempre mostraremos la fecha de los cambios más recientes con enlaces a versiones anteriores. Si realizamos cambios significativos, le notificaremos de manera más destacada.`
}
