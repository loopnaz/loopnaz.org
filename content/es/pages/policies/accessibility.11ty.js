/**
 * @file La pantilla para la página de la declaración de accesibilidad
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Encadenamiento de diseños en Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method `data` opcionales de plantillas de JavaScript en Eleventy}
 */

/**
 * Datos preliminares
 */
exports.data = {
  title: 'Declaración de accesibilidad.',
  slug: 'accesibilidad',
  templateEngineOverride: '11ty.js,md',
}

/**
 * El marcado de la página de la declaración de accesibilidad
 * @param {Object} data El objeto de `data` en Eleventy
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructurar propriedades de `data` usado en esta pantilla */
  var {
    page,
    pkg: {repository}
  } = data

  return `<!--${page.inputPath}-->
Estamos comprometidos en hacer que nuestro sitio sea accesible para todos, especialmente si tienes una discapacidad.

## Estándares mínimos

Nuestro sitio cumple o supera las [Pautas de Accesibilidad para el Contenido Web 2.1, Nivel AA][wcag2aa].

## Consideraciones adicionales

También tomamos las siguientes medidas para asegurar que nuestro sitio siga siendo accesible:

* Utilizamos un lenguaje escrito claro.
* Hacemos que la interfaz de usuario sea fácil de entender.
* Realizamos pruebas automatizadas y manuales de accesibilidad.
* Respetamos tus preferencias de sistema.
* Limitamos los tamaños de archivos y la carga de memoria del dispositivo.
* Publicamos nuestro código fuente en un repositorio público, permitiendo a cualquiera inspeccionarlo y sugerir cambios.
* Documentamos nuestro código para ser lo más transparente posible, incluso si tienes poca o ninguna experiencia en programación.

[git]: ${repository.home}
[wcag2aa]: https://www.w3.org/WAI/standards-guidelines/wcag/${page.lang}`
}
