/**
 * @file La pantilla de la página de “Quienes somos”
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */

var EleventyFetch = require('@11ty/eleventy-fetch');

/**
 * Datos preliminares
 */
exports.data = {
  title: 'Quienes somos',
  es: {
    weBelieve: 'Creemos',
    ourMission: 'Nuestra misión',
    missionStatement: 'Hacer discípulos semejantes a Cristo en todas las naciones',
    ourVision: 'Nuestra visión',
    visionStatement: 'Formar una red de iglesias compasivas en cada barrio de cada ciudad principal del mundo'
  },
  beliefsUrl: 'https://2017.manual.nazarene.org/es/wp-json/wp/v2/paragraphs?parent=10794&order=asc&orderby=menu_order',
  layout: 'who-we-are',
  slug: 'quienes-somos',
  templateEngineOverride: '11ty.js,md'
}

/**
 * El marcado de la página de “Quienes somos”
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
exports.render = data => {
  /** @type {Object} Destructure `data` properties used in this layout */
  var {page} = data

  return `<!--${page.inputPath}-->
Somos una iglesia en el Chicago Loop impulsada por la compasión por el prójimo más necesitado.

Siguiendo el ejemplo de Jesús de Nazaret, estamos aquí para buscar y servir al corazón del mundo que trabaja duro, lucha y duele.

También pertencemos a una familia global. La Iglesia del Nazareno es el hogar de más de 2,6 millones miembros que adoran en más de 30,000 congregaciones en áreas del mundo.`
}
