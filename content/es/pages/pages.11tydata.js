/**
 * @file Datos de directorio comunes a todas las páginas
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Datos de templatos y directorios en Eleventy}
 */

/**
 * Datos de directorio
 * @module content/es/pages
 */
module.exports = {
  /** @see {@link https://www.11ty.dev/docs/collections/ Colecciones en Eleventy} */
  tags: 'pages',
  /** @see {@link https://www.11ty.dev/docs/layouts/ Diseños en Eleventy} */
  layout: 'page',
  /** @see {@link https://www.11ty.dev/docs/data-computed/ Datos calculados en Eleventy} */
  eleventyComputed: {
    /**
     * Dar formato a los enlaces permanentes como `/es/slug/` o `/es/page.fileSlug`
     * @see {@link https://www.11ty.dev/docs/permalinks/ Enlaces permanentes en Eleventy}
     */
    permalink: data => `/${data.lang}/${data.slug ? data.slug : data.page.fileSlug}/index.html`
  }
}
