/**
 * @file Defines a Service Worker for the app
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @since 2.0.0
 * @see {@link https://developers.google.com/web/fundamentals/primers/service-workers “Service Workers: an Introduction” by Matt Gaunt on Web Fundamentals}
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API Service Worker API on MDN}
 * @see {@link https://gomakethings.com/series/service-workers/ Service Workers series by Chris Ferdinandi}
 */

/** @type {string} The service worker version */
var version = 'loopnaz_2.0.0'

/** @type {string} The key for core assets in CacheStorage */
var coreID = `${version}_core`

/** @type {string} The key for HTML pages in CacheStorage */
var pagesID = `${version}_pages`

/** @type {string} The key for images pages in CacheStorage */
var imagesID = `${version}_images`

/** @type {string[]} Version-appended `cacheName` */
var cacheIDs = [coreID, pagesID, imagesID]

/** @type {string[]} Relative URLs of font files */
var fontFiles = [
  '/fonts/lato/Lato-Regular-kern-latin.woff2',
  '/fonts/lato/Lato-Regular-hint-all.woff2',
  '/fonts/lato/Lato-Italic-hint-all.woff2',
  '/fonts/lato/Lato-Bold-hint-all.woff2',
  '/fonts/lato/Lato-BoldItalic-hint-all.woff2'
]

/**
 * Add core assets to CacheStorage
 * @async
 * @return {function} Add response objects to a given cache
 */
async function cacheCoreAssets() {
  /** @type {string[]} URLs to cache */
  var toCache = [
    '/icons.svg',
    '/offline/',
    ...fontFiles
  ]

  /** @type {Promvise.<Object.<Cache>> `Cache` keys}*/
  var cache = await caches.open(coreID)

  // Add each item to the cache
  return cache.addAll(toCache)
}

/**
 * Add a requested resource to `CacheStorage`
 * @async
 * @param {Object.<Request>} request  The request from a fetch event
 * @param {Object.<Response>} response The response to pair with the request
 * @param {string} cacheID The key for the target cache
 * @return {Promise} Adds request/response key/value pairs to the cache
 */
async function addToCache(request, response, cacheID) {
  /** @type {Promvise.<Object.<Cache>> `Cache` keys}*/
  var cache = await caches.open(cacheID)

  // Add both the request and its matching repsone to the cache
  return await cache.put(request, response)
}

/**
 * Remove keys from `CacheStorage`
 * @param {string[]} keys Names of `Cache` objects to remove
 * @return {void}
 */
async function removeFromCache(keys) {
  await caches.delete(key)
}

/**
 * Clear `CacheStorage`
 * @async
 * @return {function} Sets this service worker for the client
 */
async function clearCache() {
  /** @type {Promvise.<string[]> `Cache` keys}*/
  var keys = await caches.keys()

  // Remove all existing keys from the cache objects
  await Promise.all(keys.filter(key => !cacheIDs.includes(key))
    .map(removeFromCache))

  // Take control from any other registered service workers
  return self.clients.claim()
}

/**
 * Fetch a resource from the network to add to the appropriate cache
 * @async
 * @param {Object.<Event>} event A `fetch` event
 * @return {Object} The response object
 */
async function fetchFromNetwork(event) {
  /** @type {Promvise.<Object.<Response>>} The `fetch` response */
  var response = await fetch(event.request)

  // For an image, store a copy of in the images cache
  if (event.request.headers.get('Accept').includes('image')) {
    /** @type {Object.<Response>} Store a copy of the response for matching with requests */
    var copy = response.clone()

    // Add the response to the pagesID cache
    event.waitUntil(addToCache(event.request, copy, imagesID))
  }
  return response
}

/**
 * Cache items fetched from the network
 * @async
 * @param {Object.<Event>} event A `fetch` event
 * @return {Object|Promise} The response object or the cached object
 */
async function networkFirst(event) {
  try {
    /** @type {Promise.Object.<Response>>} The `fetch` response */
    var response = await fetch(event.request)

    // If response is not restricted
    if (response.type !== 'opaque') {
      /** @type {Object.<Response>} Store a copy of the response for matching with requests */
      var copy = response.clone()

      // Add the response to the pagesID cache
      event.waitUntil(addToCache(event.request, copy, pagesID))
    }
    return response
  } catch (error) {
    /** @type {Promvise.<Object.<Response>>} The first matching request in the `Cache` object */
    response = await caches.match(event.request)

    // Redirect the user to the offline page when there is not matching response in the cache 
    return await (response || caches.match('/offline/'))
  }
}

/**
 * Check the `CacheStorage` before fetching a resource from the network
 * @async
 * @param {Object.<Event>} event A `fetch` event
 * @return {Object|function} The cached object or the response from the network
 */
async function offlineFirst(event) {
  /** @type {Promise.<Object.<Response>>} The first matching request in the `Cache` object */
  var response = await caches.match(event.request)

  // Fetch the resource from the network when there is no matching response the cache
  return response || fetchFromNetwork(event)
}

// On install, cache core assets
self.addEventListener('install', event => {
  // The promise that skipWaiting() returns can be safely ignored
  self.skipWaiting()
  event.waitUntil(cacheCoreAssets())
})

// On version update, clear stale `CacheStorage`
self.addEventListener('activate', event => event.waitUntil(clearCache()))

// On fetch request, coordinate responses and cached assets
self.addEventListener('fetch', event => {
  /** @type {Object.<Request>} The resource request from the fetch event  */
  var request = event.request

  /*
   * Bug: Hide 'only-if-cached' fetch error
   * @see {@link https://github.com/paulirish/caltrainschedule.io/pull/51/commits CORS–DevTools bug fix Paul Irish on GitHub}
   */
  if(request.cache === 'only-if-cached' && request.mode !== 'same-origin') return

  // Ignore requests besides GET
  if(request.method !== 'GET') return

  // Is the response an HTML page?
  if(request.headers.get('Accept').includes('text/html')) {
    // Follow a network-first approach to caching
    return event.respondWith(networkFirst(event))
  }

  // Is the response an image?
  if(request.headers.get('Accept').includes('image')
    // Or a font?
    || request.url.includes('Lato')) {
    // Follow an offline-first approach to caching
    return event.respondWith(offlineFirst(event))
  }

  return
})
